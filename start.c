#include "clock.h"
#include "process.h"

extern void clear_screen();
extern void place_cursor(uint32_t, uint32_t);

void kernel_start(void) {
    clear_screen();
    place_cursor(4, 0);
    init_clock();
    init_processes();
}
