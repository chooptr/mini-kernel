#ifndef __PROCESS_H__
#define __PROCESS_H__

#include <cpu.h>
#include <inttypes.h>
#include <stdio.h>

#define REG_TAB_LEN 5
#define MAX_NUM_PRC 8
#define PRC_NAM_LEN 16
#define EXE_STK_LEN 512

extern void ctx_sw(int32_t, int32_t);

enum Priority {
    NO_PRIORITY = 0, LOW = 2, MEDIUM = 4, HIGH = 6
};

enum State {
    RUNNING, READY, SLEEPING, ZOMBIE
};

struct Process {
    int32_t pid;
    int32_t wake_time;
    int32_t regs[REG_TAB_LEN];
    int32_t *exec_stack;
    char name[PRC_NAM_LEN];
    enum State proc_state;
    enum Priority proc_priority;
    struct Process *next;
};

//struct Process *ready_head, *ready_tail;

// Index 0 and 1 for idle, 2 and 3 for low, 4 and 5 for medium and 6 and 7 for high
struct Process **ready_queues;

//struct Process *ready_head, *ready_tail; //(Pour le process sans priorité)

struct Process *zombie_head, *zombie_tail;

struct Process *sleeping_head;

struct Process *running_process;

void kill();

void schedule();

void pop_ready();

void sleep(int32_t);

void init_processes();

void system_monitor();


char *get_name();


int32_t get_pid();

int32_t get_seconds();

//int32_t create_process(void (*)(), char *);

int32_t create_process(void (*)(), char *, enum Priority);

#endif /* __PROCESS_H__ */