#include "static_process.h"

void static_idle() {
    for (;;) {
        printf("[%s] pid = %d \n", static_get_name(), static_get_pid());
        static_scheduling();
    }
}

void static_proc1() {
    for (;;) {
        printf("[%s] pid = %d\n", static_get_name(), static_get_pid());
        static_scheduling();
    }
}

void static_scheduling() {
    static_running_proc->proc_state = READY;
    int32_t cur_pid = static_running_proc->pid;
    static_running_proc = &static_processes[(cur_pid + 1) % PRC_NUM_LEN];
    static_running_proc->proc_state = RUNNING;
    ctx_sw((int32_t) static_processes[cur_pid].regs,
           (int32_t) static_processes[static_running_proc->pid].regs);
}

char* static_get_name() {
    return static_running_proc->name;
}

int32_t static_get_pid() {
    return static_running_proc->pid;
}

void init_static_processes(){
    static_processes[0].pid = 0;
    sprintf(static_processes[0].name, "idle");
    static_processes[0].proc_state = RUNNING;

    static_processes[1].pid = 1;
    sprintf(static_processes[1].name, "proc1");
    static_processes[1].proc_state = READY;
    static_processes[1].exec_stack[STK_EXE_LEN - 1] = (int32_t) &static_proc1;
    static_processes[1].regs[1] = (int32_t) &static_processes[1].exec_stack[STK_EXE_LEN - 1];

    static_running_proc = &static_processes[0];
    static_idle();
}
