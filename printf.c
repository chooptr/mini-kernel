/*
 * Copyright (C) 2002, Simon Nieuviarts.
 */
/*
 * Copyright (c) 1994-1999 The University of Utah and the Flux Group.
 * All rights reserved.
 * 
 * This file is part of the Flux OSKit.  The OSKit is free software, also known
 * as "open source;" you can redistribute it and/or modify it under the terms
 * of the GNU General Public License (GPL), version 2, as published by the Free
 * Software Foundation (FSF).  To explore alternate licensing terms, contact
 * the University of Utah at csl-dist@cs.utah.edu or +1-801-585-3271.
 * 
 * The OSKit is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GPL for more details.  You should have
 * received a copy of the GPL along with the OSKit; see the file COPYING.  If
 * not, write to the FSF, 59 Temple Place #330, Boston, MA 02111-1307, USA.
 */

/* 
 * Mach Operating System
 * Copyright (c) 1993,1991,1990,1989 Carnegie Mellon University
 * All Rights Reserved.
 * 
 * Permission to use, copy, modify and distribute this software and its
 * documentation is hereby granted, provided that both the copyright
 * notice and this permission notice appear in all copies of the
 * software, derivative works or modified versions, and any portions
 * thereof, and that both notices appear in supporting documentation.
 * 
 * CARNEGIE MELLON ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS"
 * CONDITION.  CARNEGIE MELLON DISCLAIMS ANY LIABILITY OF ANY KIND FOR
 * ANY DAMAGES WHATSOEVER RESULTING FROM THE USE OF THIS SOFTWARE.
 * 
 * Carnegie Mellon requests users of this software to return to
 * 
 *  Software Distribution Coordinator  or  Software.Distribution@CS.CMU.EDU
 *  School of Computer Science
 *  Carnegie Mellon University
 *  Pittsburgh PA 15213-3890
 * 
 * any improvements or extensions that they make and grant Carnegie Mellon
 * the rights to redistribute these changes.
 */

#include "stdarg.h"
#include "doprnt.h"
#include "console.h"
#include "inttypes.h"
#include "stdio.h"
#include "cpu.h"
#include "string.h"

/*
 * This is the function called by printf to send its output to the screen. You
 * have to implement it in the kernel.
 */
extern void console_putbytes(const char *s, int len);

/* This version of printf is implemented in terms of putchar and puts.  */

#define    PRINTF_BUFMAX    128


uint32_t cursorLig = 0;
uint32_t cursorCol = 0;

uint8_t Background = 11; // you can change the color
uint8_t TextColor = 15; // you can change the color

uint8_t make_color(uint8_t fg, uint8_t bg) {
    // On met le plus grand bit toujours a 0
    return (fg | bg << 4) & (0x7F);
}

uint8_t get_attribute() {
    return make_color(TextColor, Background);
}

void set_attribute(uint8_t fg, uint8_t bg) {
    TextColor = fg;
    Background = bg;
}

uint16_t *ptr_mem(uint32_t lig, uint32_t col) {
    uint16_t *address = (uint16_t *) (0xB8000 + 2 * (lig * 80 + col));
    return address;
}

uint16_t get_cursor() {
    return (cursorLig * 80 + cursorCol);
}

void place_cursor(uint32_t lig, uint32_t col) {
    cursorLig = lig;
    cursorCol = col;
    uint16_t pos = get_cursor();
    uint8_t partie_basse = pos & 0xff;
    uint8_t partie_haute = (pos >> 8) & 0xff;
    outb(0x0F, 0x3D4);
    outb(partie_basse, 0x3D5);
    outb(0x0E, 0x3D4);
    outb(partie_haute, 0x3D5);
}

void write_char(uint32_t lig, uint32_t col, char c) {
    uint16_t *address = ptr_mem(lig, col);
    uint16_t couleur = make_color(TextColor, Background);
    uint16_t c16 = c;
    uint16_t toWrite = c16 | (couleur << 8);
    *address = toWrite;
}

void clear_line(uint16_t l) {
    for (int i = 0; i < 80; i++) {
        write_char(l, i, ' ');
    }
}

void scrolling() {
    memmove((void *) 0xB8000 + 8 * 80, (void *) (0xB8000 + 10 * 80), 2 * (23 * 80 + 80));
    clear_line(24);
}

void clear_screen() {
    for (int i = 0; i < 25; i++) {
        for (int j = 0; j < 80; j++) {
            write_char(i, j, ' ');
        }
    }
}

void char_treatment(char c) {
    if (c >= 32 && c <= 126) {
        write_char(cursorLig, cursorCol, c);
        if (cursorCol == 79) {
            cursorCol = 0;
            if (cursorLig == 24) {
                scrolling();
            } else { cursorLig++; }
        } else { cursorCol++; }
        place_cursor(cursorLig, cursorCol);
    }

    if (c == 8) {
        if (cursorCol != 0) {
            --cursorCol;
            place_cursor(cursorLig, cursorCol);
            write_char(cursorLig, cursorCol, ' ');
        }
    }
    if (c == 9) {
        if (cursorCol < 72) {
            cursorCol += 8;
        } else { cursorCol = 79; }
        place_cursor(cursorLig, cursorCol);
    }
    if (c == 10) {
        cursorCol = 0;
        if (cursorLig == 24) {
            scrolling();
        } else { cursorLig++; }
        place_cursor(cursorLig, cursorCol);
    }
    if (c == 12) {
        clear_screen();
        cursorLig = 0;
        cursorCol = 0;
        place_cursor(cursorLig, cursorCol);
    }
    if (c == 13) {
        cursorCol = 0;
        place_cursor(cursorLig, cursorCol);
    }
}

void console_putbytes(const char *chaine, int taille) {
    for (int i = 0; i < taille; i++) {
        char_treatment(chaine[i]);
    }
}

void displayAt(uint16_t lig, uint16_t col, const char *s, uint8_t bg, uint8_t fg) {
    uint8_t bg_tmp = Background;
    uint8_t fg_tmp = TextColor;
    Background = bg;
    TextColor = fg;
    uint32_t tmpLig = cursorLig;
    uint32_t tmpCol = cursorCol;
    place_cursor(lig, col);
    for (int i = 0; i < strlen(s); i++) {
        char_treatment(s[i]);
    }
    cursorLig = tmpLig;
    cursorCol = tmpCol;
    Background = bg_tmp;
    TextColor = fg_tmp;
}

struct printf_state {
    char buf[PRINTF_BUFMAX];
    unsigned int index;
};

static void
flush(struct printf_state *state) {
    /*
     * It would be nice to call write(1,) here, but if fd_set_console
     * has not been called, it will break.
     */
    console_putbytes((const char *) state->buf, state->index);

    state->index = 0;
}

static void
printf_char(arg, c)
        char *arg;
        int c;
{
    struct printf_state *state = (struct printf_state *) arg;

    if ((c == 0) || (c == '\n') || (state->index >= PRINTF_BUFMAX)) {
        flush(state);
        state->buf[0] = c;
        console_putbytes((const char *) state->buf, 1);
    } else {
        state->buf[state->index] = c;
        state->index++;
    }
}

/*
 * Printing (to console)
 */
int vprintf(const char *fmt, va_list args) {
    struct printf_state state;

    state.index = 0;
    _doprnt(fmt, args, 0, (void (*)()) printf_char, (char *) &state);

    if (state.index != 0)
        flush(&state);

    /* _doprnt currently doesn't pass back error codes,
       so just assume nothing bad happened.  */
    return 0;
}

int
printf(const char *fmt, ...) {
    va_list args;
    int err;

    va_start(args, fmt);
    err = vprintf(fmt, args);
            va_end(args);

    return err;
}

int putchar(int c) {
    char ch = c;
    console_putbytes(&ch, 1);
    return (unsigned char) ch;
}

int puts(const char *s) {
    while (*s) {
        putchar(*s++);
    }
    putchar('\n');
    return 0;
}
