# Mini-kernel

This is a simple kernel written from scratch in C. Functionalities include a clock, text display and round-robin scheduling.
