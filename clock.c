#include "clock.h"
#include "process.h"


int8_t seconds = 0;
int8_t minutes = 0;
int8_t hours = 0;
int32_t system_pulse = 0;
int32_t CLOCKFREQ = 50;
int32_t QUARTZ = 0x1234DD;


void display_time(const char *time) {
    displayAt(0, 80 - strlen(time), time, 11, 7);
}

void tic_PIT() {
    outb(0x20, 0x20);
    char date[9];
    system_pulse++;
    if (system_pulse != 0 && system_pulse % 50 == 0) {
        seconds++;
        if (seconds % 60 == 0) {
            seconds = 0;
            minutes++;
            if (minutes % 60 == 0) {
                minutes = 0;
                hours++;
            }
        }
    }
    sprintf(date, "%02d:%02d:%02d", hours, minutes, seconds);
    display_time(date);
    schedule();
}

void init_traitant_IT(int32_t num_IT, void (*traitant)()) {
    int32_t first = (((int32_t) KERNEL_CS) << 16) | ((int32_t) traitant & 0x0000ffff);
    int32_t second = ((int32_t) traitant & 0xffff0000) | ((int32_t) 0x8E00 & 0x0000ffff);
    int32_t *addr = ((int32_t *) (0x1000 + 8 * num_IT));
    *addr = first;
    *(addr + 1) = second;
}

void adjust_freq() {
    outb(0x34, 0x43);
    outb((QUARTZ / CLOCKFREQ) % 256, 0x40);
    outb(((QUARTZ / CLOCKFREQ) >> 8) & 0x0000ffff, 0x40);
}

void mask_IRQ(int32_t num_IRQ, bool mask) {
    int8_t in = inb(0x21);
    int8_t out = (int8_t) (mask ? in | (1 << num_IRQ) : in & (0xff ^ (1 << num_IRQ)));
    outb(out, 0x21);
}

void init_clock() {
    adjust_freq();
    init_traitant_IT(32, traitant_IT_32);
    mask_IRQ(0, false);
}

int32_t system_time() {
    return seconds + 60 * minutes + 3600 * hours;
}
