#ifndef __STATIC_PROCESS_H__
#define __STATIC_PROCESS_H__

#include <cpu.h>
#include <inttypes.h>
#include <stdio.h>

#define PRC_NUM_LEN 2
#define REG_TAB_LEN 5
#define PRC_NAM_LEN 16
#define STK_EXE_LEN 512

extern void ctx_sw(int32_t, int32_t);

enum static_state {RUNNING, READY};

struct StaticProcess {
	int32_t pid;
	char name[PRC_NAM_LEN];
	enum static_state proc_state;
	int32_t regs[REG_TAB_LEN];
	int32_t exec_stack[STK_EXE_LEN];
};

struct StaticProcess* static_running_proc;

struct StaticProcess static_processes[PRC_NUM_LEN];

void init_static_processes();

void static_idle();

void static_proc1();

void static_scheduling();

char* static_get_name();

int32_t static_get_pid();

#endif /* __STATIC_PROCESS_H__ */
