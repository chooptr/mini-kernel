#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <cpu.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <segment.h>
#include <stdbool.h>

extern void traitant_IT_32();

extern void displayAt(uint16_t lig, uint16_t col, const char *s, uint8_t, uint8_t);


void tic_PIT();

void init_clock();

void adjust_freq();

void display_time(const char *);

void mask_IRQ(int32_t num_IRQ, bool masque);

void init_traitant_IT(int32_t num_IT, void (*traitant)());


int32_t system_time();

#endif /* __CLOCK_H__*/
