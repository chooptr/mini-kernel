#include "process.h"
#include "clock.h"
#include "malloc.c.h"

int32_t cur_number_procs = 0;

void idle() {
    for (;;) {
        sti();
        hlt();
        cli();
    }
}

void test(){
    for(int i = 0; i < 4; ++i){
        printf("this is %s at iteration %d\n", get_name(), i);
        sleep(1);
    }
}

void proc1() {
    for (int i = 0; i < 2; ++i) {
        printf("[temps = %d] processus %s pid = %i\n", get_seconds(),
               get_name(), get_pid());
        sleep(2);
    }
}

void proc2() {
    for (int i = 0; i < 5; ++i) {
        printf("[temps = %d] processus %s pid = %i\n", get_seconds(),
               get_name(), get_pid());
        sleep(3);
    }
}

void proc3() {
    for (int i = 0; i < 2; ++i) {
        printf("[temps = %d] processus %s pid = %i\n", get_seconds(),
               get_name(), get_pid());
        sleep(2);
    }
    create_process(test, "test", MEDIUM);
}

void push_ready(struct Process* process) {
    if (!ready_queues[process->proc_priority]) {
        ready_queues[process->proc_priority] = process;
        ready_queues[process->proc_priority + 1] = process;
    } else {
        ready_queues[process->proc_priority + 1]->next = process;
        ready_queues[process->proc_priority + 1] = process;
    }
    process->proc_state = READY;
}

void pop_ready(){
    int32_t index = ready_queues[6] ? 6 : (ready_queues[4] ? 4 : (ready_queues[2] ? 2 : 0));
    running_process = ready_queues[index];
    ready_queues[index] = ready_queues[index]->next;
    running_process->proc_state = RUNNING;
    running_process->next = NULL;
}

void insert_sleeping(struct Process *process) {
    process->proc_state = SLEEPING;
    if (!sleeping_head) {
        sleeping_head = process;
        return;
    }
    if (sleeping_head->wake_time > process->wake_time) {
        process->next = sleeping_head;
        sleeping_head = process;
        return;
    }
    struct Process *tmp = sleeping_head;
    while (tmp->next && tmp->next->wake_time < process->wake_time) {
        tmp = tmp->next;
    }
    process->next = tmp->next;
    tmp->next = process;
}

void wake_sleeping() {
    struct Process* tmp;
    while (sleeping_head && sleeping_head->wake_time <= system_time()) {
        tmp = sleeping_head->next;
        sleeping_head->proc_state = READY;
        sleeping_head->next = NULL;
        push_ready(sleeping_head);
        sleeping_head = tmp;
    }
}

int32_t create_process(void (*code)(), char *name, enum Priority priority) {
    if(cur_number_procs > MAX_NUM_PRC) return -1;

    struct Process *dp = malloc(sizeof(struct Process));
    dp->pid = cur_number_procs++;
    sprintf(dp->name, name);
    dp->proc_state = READY;
    dp->proc_priority = priority;
    dp->exec_stack = malloc(sizeof(int32_t) * EXE_STK_LEN);
    dp->exec_stack[EXE_STK_LEN - 1] = (int32_t) kill;
    dp->exec_stack[EXE_STK_LEN - 2] = (int32_t) code;
    dp->regs[1] = (int32_t) &dp->exec_stack[EXE_STK_LEN - 2];

    push_ready(dp);

    return dp->pid;
}

void kill_zombies() {
    struct Process* tmp;
    while(zombie_head){
        tmp = zombie_head->next;
        free(zombie_head);
        zombie_head = tmp;
    }
}

void schedule() {
    system_monitor();
    wake_sleeping();
    kill_zombies();
    struct Process *tmp = running_process;
    if (running_process->proc_state == RUNNING) {
        push_ready(running_process);
    }
    pop_ready();
    ctx_sw((int32_t) tmp->regs, (int32_t) running_process->regs);
}

void sleep(int32_t seconds) {
    running_process->wake_time = system_time() + seconds;
    insert_sleeping(running_process);
    sti();
    hlt();
    cli();
}

char *get_name() {
    return running_process->name;
}

int32_t get_pid() {
    return running_process->pid;
}

int32_t get_seconds() {
    return running_process->wake_time;
}

void init_running_process(){
    running_process = ready_queues[0];
    ready_queues[0] = NULL;
    ready_queues[1] = NULL;
    running_process->proc_state = RUNNING;
    running_process->next = NULL;
}

void init_processes() {
    ready_queues = malloc(sizeof(int32_t) * 8);
    printf("idle created with pid %d\n", create_process(idle, "idle", NO_PRIORITY));
    printf("proc1 created with pid %d\n", create_process(proc1, "proc1", LOW));
    printf("proc2 created with pid %d\n", create_process(proc2, "proc2", MEDIUM));
    printf("proc3 created with pid %d\n", create_process(proc3, "proc3", HIGH));

    init_running_process();
    idle();
}

void push_zombie(struct Process* process){
    process->proc_state = ZOMBIE;
    if (!zombie_head) {
        zombie_head = process;
        zombie_tail = process;
    } else {
        zombie_tail->next = process;
        zombie_tail = process;
    }
}

void kill(){
    push_zombie(running_process);
    sti();
    hlt();
    cli();
}

extern void clear_line(int32_t);

void get_process_list(struct Process* process, char* to_display){
    struct Process* iterator = process;
    while(iterator){
        sprintf(to_display + strlen(to_display), " %d", iterator->pid);
        iterator = iterator->next;
    }
}

void display_list(struct Process* process, char* state, int32_t line, int32_t column, int32_t fg){
    char to_display[80];
    sprintf(to_display, "%s: ", state);
    get_process_list(process, to_display);
    clear_line(line);
    displayAt(line, column, to_display, 11, fg);
}

void display_ready(){
    char to_display[80];
    sprintf(to_display, "%s: ", "Ready");
    get_process_list(ready_queues[0], to_display);
    get_process_list(ready_queues[2], to_display);
    get_process_list(ready_queues[4], to_display);
    get_process_list(ready_queues[6], to_display);
    clear_line(1);
    displayAt(1, 0, to_display, 11, 4);
}

void system_monitor(){
    char running[30];
    sprintf(running, "Running: %d", running_process->pid);
    displayAt(0, 0, running, 11, 14);
    display_ready();
    display_list(sleeping_head, "Sleeping", 2, 0, 1);
    display_list(zombie_head, "Zombie", 3, 0, 0);
}
